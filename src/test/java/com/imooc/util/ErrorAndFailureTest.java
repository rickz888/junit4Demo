package com.imooc.util;

import com.imooc.util.Calculate;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * 1.Failure一般由单元测试使用的断言方法判断失败所引起的，这经表示 测试点发现了问题，
 *  就是说程序输出的结果和我们的预期的不一样。
 * 2.error是由代码异常引起的，它可以产生于测试代码本身的错误，也可以是被测试代码中的一个隐藏的bug
 * 3.测试用例不是用来证明你是对的，而是用来证明你没有错。
 */
public class ErrorAndFailureTest {

    @Test
    public void testAdd(){
        assertEquals(5, new Calculate().add(3,3));
    }

    @Test
    public void testDevide(){
        assertEquals(22, new Calculate().devide(66,0));
    }

}
