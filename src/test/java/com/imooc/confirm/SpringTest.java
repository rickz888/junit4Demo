package com.imooc.confirm;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Date;

import static org.junit.Assert.*;

public class SpringTest {

    private static ApplicationContext context = null;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception{
        context = new ClassPathXmlApplicationContext("spring-config.xml");
    }

    @Test
    public void test(){
        Date date = (Date) context.getBean("date");
        System.out.println(date);
    }

}